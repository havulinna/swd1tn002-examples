package week5.maps;

import java.util.HashMap;
import java.util.Map;

public class MapEsimerkki {

    public static void main(String[] args) {
        Map<String, String> sanakirja = new HashMap<String, String>();

        sanakirja.put("kissa", "cat"); // lisää arvo "cat" avaimelle "kissa"
        sanakirja.put("koira", "dog"); // lisää arvo "dog" avaimelle "koira"

        String kaannos = sanakirja.get("koira"); // palauttaa arvon "dog"
        System.out.println(kaannos);

        sanakirja.remove("koira"); // poistaa avain-arvo parin "koira", "dog"
    }
}
