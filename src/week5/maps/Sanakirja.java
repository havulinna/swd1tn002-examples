package week5.maps;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Sanakirja {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Map<String, String> sanat = new HashMap<String, String>();
        boolean stop = false;

        while (!stop) {
            System.out.println("Anna seuraava komento:");
            String rivi = input.nextLine();
            String[] osat = rivi.split(" ");
            String komento = osat[0];

            switch (komento) {
            case "lisää":
                lisaaSanaPari(sanat, osat[1], osat[2]);
                break;
            case "kaikki":
                System.out.println(sanat);
                break;
            case "käännä":
                kaannaSana(sanat, osat[1]);
                break;
            case "poista":
                poistaSana(sanat, osat[1]);
                break;
            case "lopeta":
                stop = true;
                break;
            default:
                System.out.println("Tuntematon komento");
                break;
            }
        }
        input.close();
    }

    private static void poistaSana(Map<String, String> sanat, String fin) {
        if (sanat.containsKey(fin)) {
            sanat.remove(fin);
            System.out.println("Poistettiin " + fin);
        } else {
            System.out.println("Sanaa " + fin + " ei löytynyt");
        }
    }

    private static void kaannaSana(Map<String, String> sanat, String fin) {
        if (sanat.containsKey(fin)) {
            System.out.println(fin + " => " + sanat.get(fin));
        } else {
            System.out.println("Sanaa " + fin + " ei löytynyt");
        }
    }

    private static void lisaaSanaPari(Map<String, String> sanakirja, String fin, String eng) {
        sanakirja.put(fin, eng);
        System.out.println("Lisättiin " + fin + " => " + eng);
    }
}
