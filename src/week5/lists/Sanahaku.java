package week5.lists;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Sanahaku {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Kirjoita sanoja (monta samalle riville):");
        String rivi = input.nextLine();

        String[] sanat = rivi.split(" ");
        List<String> sanalista = Arrays.asList(sanat);

        System.out.print("Anna etsittävä sana: ");
        String etsittavaSana = input.nextLine();

        if (listaSisaltaaSanan(sanalista, etsittavaSana)) {
            System.out.println("Sana " + etsittavaSana + " löytyi listasta");
        } else {
            System.out.println("Sanaa " + etsittavaSana + " ei löytynyt listasta");
        }
        input.close();
    }

    private static boolean listaSisaltaaSanan(List<String> sanalista, String etsittavaSana) {
        for (String s : sanalista) {
            if (s.equalsIgnoreCase(etsittavaSana)) {
                return true;
            }
        }
        return false;
    }
}
