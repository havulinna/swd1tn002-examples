package week4.objects;

public class Laskuri {

    private int luku;

    public Laskuri() {
        this.luku = 0;
    }

    public void kasvata() {
        this.luku++;
    }

    public int kerroLuku() {
        return this.luku;
    }
}
