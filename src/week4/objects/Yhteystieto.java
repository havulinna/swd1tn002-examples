package week4.objects;

public class Yhteystieto {

    private String nimi;
    private String puhelinnumero;
    private String email;

    public Yhteystieto() {
        this.nimi = null;
        this.puhelinnumero = null;
        this.email = null;
    }

    public Yhteystieto(String nimi, String puhelinnumero, String email) {
        this.nimi = nimi;
        this.puhelinnumero = puhelinnumero;
        this.email = email;
    }

    public String getNimi() {
        return nimi;
    }

    public void setNimi(String nimi) {
        this.nimi = nimi;
    }

    public String getPuhelinnumero() {
        return puhelinnumero;
    }

    public void setPuhelinnumero(String puhelinnumero) {
        this.puhelinnumero = puhelinnumero;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        String output = "Nimi: " + this.nimi;
        if (email != null) {
            output += ", email: " + email;
        }
        if (puhelinnumero != null) {
            output += ", puhelin: " + puhelinnumero;
        }
        return output;
    }
}