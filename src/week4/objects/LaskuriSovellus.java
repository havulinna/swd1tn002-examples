package week4.objects;

public class LaskuriSovellus {

    public static void main(String[] args) {
        Laskuri laskuri1 = new Laskuri();
        Laskuri laskuri2 = new Laskuri();

        laskuri1.kasvata();
        laskuri1.kasvata();
        laskuri1.kasvata();
        laskuri1.kasvata();

        laskuri2.kasvata();

        System.out.println("Laskuri 1: " + laskuri1.kerroLuku());
        System.out.println("Laskuri 2: " + laskuri2.kerroLuku());
    }
}
