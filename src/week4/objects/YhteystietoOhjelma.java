package week4.objects;

public class YhteystietoOhjelma {

    public static void main(String[] args) {

        Yhteystieto matti = new Yhteystieto();
        matti.setNimi("Matti Meikäläinen");
        matti.setPuhelinnumero("+35840555555");
        matti.setEmail("matti@example.com");

        Yhteystieto maija = new Yhteystieto("Maija Meikäläinen", null, "maija@example.com");

        System.out.println(matti);
        System.out.println(maija);
    }
}
