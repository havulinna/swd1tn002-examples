package week4.objects.phonebook;

import week4.objects.Yhteystieto;

public class Puhelinluettelo {

    private Yhteystieto[] yhteystiedot;

    public Puhelinluettelo() {
        this.yhteystiedot = new Yhteystieto[1000];
    }

    public Yhteystieto etsiYhteystieto(String nimi) {
        for (Yhteystieto y : this.yhteystiedot) {
            if (y != null && nimi.equalsIgnoreCase(y.getNimi())) {
                return y;
            }
        }
        return null;
    }

    public void lisaaYhteystieto(Yhteystieto uusi) {
        for (int i = 0; i < yhteystiedot.length; i++) {
            if (yhteystiedot[i] == null) {
                yhteystiedot[i] = uusi;
                return;
            }
        }
    }
}
