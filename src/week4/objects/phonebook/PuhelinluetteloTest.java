package week4.objects.phonebook;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import week4.objects.Yhteystieto;

public class PuhelinluetteloTest {

    private Puhelinluettelo pl = new Puhelinluettelo();
    private Yhteystieto matti = new Yhteystieto("Matti Meikäläinen", "+35840555555", "matti@example.com");
    private Yhteystieto maija = new Yhteystieto("Maija Meikäläinen", null, "maija@example.com");

    @Before
    public void setUp() {
        pl.lisaaYhteystieto(matti);
        pl.lisaaYhteystieto(maija);
    }

    @Test
    public void contactIsFoundByName() {
        assertEquals(matti, pl.etsiYhteystieto("matti meikäläinen"));
    }

    @Test
    public void nullIsReturnedWhenContactIsNotFound() {
        assertEquals(null, pl.etsiYhteystieto("anonymous"));
    }
}
