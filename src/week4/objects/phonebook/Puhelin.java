package week4.objects.phonebook;

public class Puhelin {

    private Puhelinluettelo puhelinluettelo;

    public Puhelin() {
        this.puhelinluettelo = new Puhelinluettelo();
    }

    public Puhelinluettelo getPuhelinluettelo() {
        return puhelinluettelo;
    }
}
