package week4.dates;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class CreatingLocalTimes {

    public static void main(String[] args) {

        LocalDate today = LocalDate.now();
        System.out.println(today);

        LocalDateTime now = LocalDateTime.now();
        System.out.println(now);

        LocalDate date1 = LocalDate.of(1917, 12, 6);
        System.out.println(date1);

        LocalDate date2 = LocalDate.parse("1917-12-06");
        System.out.println(date2);
    }
}
