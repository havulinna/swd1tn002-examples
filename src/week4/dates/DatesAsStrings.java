package week4.dates;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DatesAsStrings {

    public static void main(String[] args) {

        // Formaatti, jossa on päivä, kuukausi ja vuosi pisteellä erotettuna
        DateTimeFormatter formaatti = DateTimeFormatter.ofPattern("d.M.yyyy");

        LocalDate today = LocalDate.now();

        // Päivämäärän näyttäminen merkkijonona:
        String pvm = today.format(formaatti);
        System.out.println(pvm);

        // Merkkijonon "parsiminen" LocalDate-objektiksi:
        LocalDate date = LocalDate.parse("6.12.1917", formaatti);
        System.out.println(date);
    }
}
