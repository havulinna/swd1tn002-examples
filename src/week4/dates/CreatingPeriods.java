package week4.dates;

import java.time.LocalDate;
import java.time.Period;

public class CreatingPeriods {

    public static void main(String[] args) {
        LocalDate independence = LocalDate.of(1917, 12, 6);
        LocalDate today = LocalDate.now();

        Period period = Period.between(independence, today);

        int years = period.getYears();
        int months = period.getMonths();
        int days = period.getDays();
        System.out.println(years + " v, " + months + " kk, " + days + " pv");
    }
}
