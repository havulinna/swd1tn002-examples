package week4.dates;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class DateDiffCalculator {

    public static void main(String[] args) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy");

        Scanner input = new Scanner(System.in);
        System.out.print("Anna päivämäärä muodossa pp.kk.vvvv: ");
        String dateString = input.nextLine();

        LocalDate today = LocalDate.now();
        LocalDate givenDate = LocalDate.parse(dateString, formatter);

        Period periodBetween = Period.between(givenDate, today);
        if (periodBetween.isNegative()) {
            periodBetween = periodBetween.negated();
        }

        System.out.println("Ero kuluvaan päivään on " + periodBetween.getYears() + " vuotta, " + periodBetween.getMonths()
                + " kuukautta ja " + periodBetween.getDays() + " päivää.");
        input.close();
    }
}
