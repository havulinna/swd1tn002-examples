package week4.dates;

import java.time.LocalDate;

public class CountingDays {

    public static void main(String[] args) {
        LocalDate nextWeek = LocalDate.now().plusDays(7);
        LocalDate yesterday = LocalDate.now().minusDays(1);

        if (yesterday.isBefore(nextWeek)) {
            System.out.println("Yesterday was before next week");
        }

        if (nextWeek.isAfter(yesterday)) {
            System.out.println("Next week is after yesterday");
        }
    }
}
