package week4.dates;

import java.time.LocalDate;

public class YearsMonthsAndDays {

    public static void main(String[] args) {

        LocalDate today = LocalDate.now();

        int year = today.getYear();
        int month = today.getMonthValue();
        int day = today.getDayOfMonth();

        System.out.println("Year " + year);
        System.out.println("Month " + month);
        System.out.println("Day " + day);
    }
}
