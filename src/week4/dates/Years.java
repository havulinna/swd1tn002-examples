package week4.dates;

import java.time.Year;

public class Years {

    public static void main(String[] args) {

        // Nykyinen vuosi
        Year thisYear = Year.now();
        int yearNumber = thisYear.getValue();

        // Vuosi 1917
        Year anotherYear = Year.of(1917);

        // Karkausvuoden selvittäminen
        boolean karkausvuosi = Year.of(2020).isLeap();
    }
}
