package week4.recap;

import java.util.Arrays;

public class LeapYears {

    public static int[] findLeapYears(int[] years) {
        int[] leapYears = new int[years.length];
        int count = 0;

        for (int current : years) {
            if (isLeapYear(current)) {
                leapYears[count] = current;
                count++;
            }
        }
        return Arrays.copyOf(leapYears, count);
    }

    public static boolean isLeapYear(int year) {
        if (year % 4 == 0 && (year % 400 == 0 || year % 100 != 0)) { 
            return true;
        } else {
            return false;
        }
    }
}
