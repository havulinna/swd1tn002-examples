package week4.recap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Test;

public class LeapYearsTest {

    @Test
    public void yearsDivisibleBy100() { 
        assertFalse(LeapYears.isLeapYear(2100));
    }

    @Test
    public void yearsDivisibleBy400() {
        assertTrue(LeapYears.isLeapYear(2000));
    }


    @Test
    public void yearsDivisibleBy4() {
        assertTrue(LeapYears.isLeapYear(2020));
    }

    @Test
    public void emptyArrayContainsNoLeapYears() {
        int[] years = new int[] {};
        int[] result = LeapYears.findLeapYears(years);

        assertEquals(0, result.length);
    }

    @Test
    public void leapYearsAreFoundAndReturned() {
        int[] years = new int[] { 2000, 2003, 2004, 2020, 2100 };
        int[] result = LeapYears.findLeapYears(years);

        assertEquals(3, result.length);
        assertEquals(Arrays.toString(new int[] {2000,  2004, 2020}), Arrays.toString(result));
    }
}
