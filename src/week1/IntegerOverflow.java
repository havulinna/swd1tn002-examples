package week1;

public class IntegerOverflow {
    public static void main(String[] args) {
        // suurin mahdollinen "int":
        System.out.println(2147483647);

        // liian suuri arvo "int"-tyypille, tapahtuu ylivuoto:
        System.out.println(2147483647 + 1);

        // long-tyyppiset luvut pystyvät esittämään suurempia lukuja:
        System.out.println(2147483647L + 1);
    }
}
