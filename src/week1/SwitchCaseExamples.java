package week1;

import java.util.Scanner;

public class SwitchCaseExamples {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.print("Syötä arvosana: ");

        int grade = input.nextInt();
        String feedback;

        switch (grade) {
        case 5:
            feedback = "kiitettävä";
            break;
        case 4:
        case 3:
            feedback = "hyvä";
            break;
        case 2:
        case 1:
            feedback = "tyydyttävä";
            break;
        case 0:
            feedback = "hylätty";
            break;
        default:
            feedback = "tuntematon arvosana";
            break;
        }
        System.out.println(feedback);
        input.close();
    }
}
