package week1;

import java.text.DecimalFormat;

public class DecimalFormatExample {

    public static void main(String[] args) {
        DecimalFormat twoDigits = new DecimalFormat("0.00");
        String rounded = twoDigits.format(100.123456);
        System.out.println(rounded);
    }

}
