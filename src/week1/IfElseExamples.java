package week1;

public class IfElseExamples {

    public static void main(String[] args) {
        int age = 10;

        if (age >= 18) {
            System.out.println("Täysi-ikäinen");
        } else if (age >= 15) {
            System.out.println("Yli 15-vuotias");
        } else {
            System.out.println("Alle 15-vuotias");
        }

    }

}
