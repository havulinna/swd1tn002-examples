package week1;

public class VariableExamples {

    public static void main(String[] args) {

        boolean result = 10 < 12 && 15 > 20;
        System.out.println(result);

        int a = 100;
        int b = 23;
        int c = (a + b) * a;
        System.out.println(c);

        int counter = 0;
        counter++;
        counter++;
        System.out.println(counter);

        String text = "Hello";
        String greeting = text + " world!";
        System.out.println(greeting);

    }
}
