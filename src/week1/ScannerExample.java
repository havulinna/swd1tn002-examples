package week1;

import java.util.Scanner;

public class ScannerExample {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Syötä luku, liukuluku, sana ja muuta tekstiä:");
        int number = input.nextInt();
        double decimalNumber = input.nextDouble();
        String singleWord = input.next();
        String lineOfText = input.nextLine();

        System.out.println(number);
        System.out.println(decimalNumber);
        System.out.println(singleWord);
        System.out.println(lineOfText);

        input.close();
    }

}
