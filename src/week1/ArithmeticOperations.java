package week1;

public class ArithmeticOperations {

    public static void main(String[] args) {

        System.out.print("1 + 2 == ");
        System.out.println(1 + 2);

        System.out.print("4 - 1 == ");
        System.out.println(4 - 1);

        System.out.print("2 * 4 == ");
        System.out.println(2 * 4);

        System.out.print("8 / 2 == ");
        System.out.println(8 / 2);

        System.out.print("9 % 2 == ");
        System.out.println(9 % 2);

        System.out.print("9.0 / 2 == ");
        System.out.println(9.0 / 2);

        System.out.print("9 / 2 == ");
        System.out.println(9 / 2);
    }
}
