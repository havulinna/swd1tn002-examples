package week2.strings;

public class StringFormat {

    public static void main(String[] args) {
        double number = 123.456;

        System.out.printf("Kahden desimaalin tarkkuus: %.2f\n", number);
        System.out.printf("Yhden desimaalin tarkkuus: %.1f\n", number);
        System.out.printf("Kahden desimaalin tarkkuus, vähintään 8 merkkiä: %8.2f\n", number);

        System.out.printf("Tosi: %b\n", true);
        System.out.printf("Epätosi: %b\n", false);
    }

}
