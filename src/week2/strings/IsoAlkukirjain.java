package week2.strings;
import java.util.Scanner;

public class IsoAlkukirjain {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Tämä ohjelma muuttaa tekstiin ison alkukirjaimen.");
        System.out.print("Kirjoita sana: ");

        String text = input.nextLine();

        String ekaKirjain = text.substring(0, 1);
        ekaKirjain = ekaKirjain.toUpperCase();

        String loppuosa = text.substring(1);
        loppuosa = loppuosa.toLowerCase();

        System.out.println(ekaKirjain + loppuosa);

        input.close();
    }

}
