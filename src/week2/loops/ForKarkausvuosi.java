package week2.loops;

import java.util.Scanner;

public class ForKarkausvuosi {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Anna aloitusvuosi: ");
        int aloitusVuosi = input.nextInt();

        System.out.print("Anna lopetusvuosi: ");
        int lopetusVuosi = input.nextInt();

        for (int vuosi = aloitusVuosi; vuosi <= lopetusVuosi; vuosi++) {
            if (vuosi % 4 == 0 && (vuosi % 100 != 0 || vuosi % 400 == 0)) {
                System.out.println("Vuosi " + vuosi + " on karkausvuosi");
            } else {
                System.out.println("Vuosi " + vuosi + " ei ole karkausvuosi");
            }
        }

        input.close();
    }
}
