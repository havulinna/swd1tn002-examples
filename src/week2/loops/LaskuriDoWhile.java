package week2.loops;

import java.util.Scanner;

public class LaskuriDoWhile {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        double luku = 0;
        double summa = 0;

        do {
            System.out.print("Syötä luku (0 lopettaa): ");
            luku = input.nextDouble();
            summa += luku;
        } while(luku != 0);

        System.out.println("Lukujen summa on " + summa);
        input.close();
    }
}
