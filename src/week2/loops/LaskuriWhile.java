package week2.loops;

import java.util.Scanner;

public class LaskuriWhile {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Syötä luku (0 lopettaa): ");
        double luku = input.nextDouble();
        double summa = luku;

        while (luku != 0) {
            System.out.print("Syötä luku (0 lopettaa): ");
            luku = input.nextDouble();
            summa += luku;
        }

        System.out.println("Lukujen summa on " + summa);
        input.close();
    }
}
