package week2.loops;

import java.util.Random;
import java.util.Scanner;

public class Arvaustehtava {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int arvaus;
        int oikea = new Random().nextInt(100);

        while (true) {
            System.out.print("Arvaa luku väliltä 0-99: ");
            arvaus = input.nextInt();

            if (arvaus == oikea) {
                System.out.println("Oikein!");
                break;
            } else if (arvaus < oikea) {
                System.out.println("Arvaa suurempi");
            } else {
                System.out.println("Arvaa pienempi");
            }
        }
        input.close();
    }
}
