package week2.loops;

import java.util.Scanner;

public class WhileKarkausvuosi {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        while (true) {
            System.out.print("Kirjoita vuosi: ");
            int vuosi = input.nextInt();

            if (vuosi % 4 == 0 && (vuosi % 100 != 0 || vuosi % 400 == 0)) {
                System.out.println("Vuosi " + vuosi + " on karkausvuosi");
            } else {
                System.out.println("Vuosi " + vuosi + " ei ole karkausvuosi");
            }
        }
    }
}
