package week2.loops;

import java.util.Scanner;

public class LaskuriBreak {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        double luku = 0;
        double summa = 0;

        while (true) {
            System.out.print("Syötä luku (0 lopettaa): ");
            luku = input.nextDouble();
            if (luku == 0) {
                break;
            } else {
                summa += luku;
            }
        }

        System.out.println("Lukujen summa on " + summa);
        input.close();
    }
}
