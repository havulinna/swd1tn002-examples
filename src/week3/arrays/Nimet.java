package week3.arrays;

import java.util.Arrays;
import java.util.Scanner;

public class Nimet {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String[] nimet = new String[100];

        int maara = 0;

        for (int i = 0; i < nimet.length; i++) {
            System.out.print("Anna nimi (tyhjä lopettaa): ");
            String nimi = input.nextLine();

            if (nimi.isEmpty()) {
                break;
            } else {
                nimet[i] = nimi;
                maara++;
            }
        }

        Arrays.sort(nimet, 0, maara);

        for (int i = 0; i < maara; i++) {
            System.out.println(nimet[i]);
        }

        input.close();
    }
}
