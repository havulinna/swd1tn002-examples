package week3.arrays;

public class ForEach {

    public static void main(String[] args) {
        int[] numerot = new int[] { 9, 8, 7, 1, 2, 3, 4, 5, 6 };

        // Perinteinen for-silmukka
        for (int i = 0; i < numerot.length; i++) {
            System.out.println(numerot[i]);
        }

        // For each -silmukka
        for (int numero : numerot) {
            System.out.println(numero);
        }
    }
}
