package week3.arrays;

import java.util.Arrays;

public class SortStrings {

    public static void main(String[] args) {
        String[] nimet = new String[] { "Miina", "Aamu", "Pilvi", "Vesa", "Ansa" };
        Arrays.sort(nimet);
        System.out.println(Arrays.toString(nimet));
    }
}
