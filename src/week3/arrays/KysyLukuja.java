package week3.arrays;

import java.util.Arrays;
import java.util.Scanner;

public class KysyLukuja {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double[] numerot = new double[123_456];
        int maara = 0;

        for (int i = 0; i < numerot.length; i++) {
            System.out.print("Anna numero (0 lopettaa): ");

            double numero = input.nextDouble();
            if (numero == 0) {
                break;
            } else {
                numerot[i] = numero;
                maara++;
            }
        }

        // Sijoitetaan muuttujaan uusi taulukko, jossa on vain annetut luvut
        numerot = Arrays.copyOf(numerot, maara);
        Arrays.sort(numerot);

        System.out.println("Annoit numerot:");


        for (int i = 0; i < numerot.length; i++) {
            System.out.println(i + ": " + numerot[i]);
        }

        input.close();
    }
}
