package week3.methods;

public class Capitalizer {

    public static String capitalize(String text) {
        String firstCharacter = text.substring(0, 1);
        String theRest = text.substring(1);
        return firstCharacter.toUpperCase() + theRest;
    }
}
