package week3.methods.tulostus;

import java.util.Scanner;

public class Ohjelma {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Kirjoita monta sanaa samalle riville:");
        String rivi = input.nextLine();
        String[] taulukko = rivi.split(" ");

        Tulostin.tulostaJarjestyksessa(taulukko);

        input.close();
    }
}
