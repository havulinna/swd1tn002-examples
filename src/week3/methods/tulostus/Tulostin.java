package week3.methods.tulostus;

import java.util.Arrays;

public class Tulostin {

    public static void tulostaJarjestyksessa(String[] sanat) {
        String[] kopio = sanat.clone();
        Arrays.sort(kopio);
        System.out.println(Arrays.toString(kopio));
    }
}
