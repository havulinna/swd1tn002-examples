package week3.methods;

public class Tervehdys {

    public static void tervehdi(String nimi) {
        System.out.println("Terve " + nimi + "!");
    }

    public static void main(String[] args) {
        // Luokan sisällä metodia voidaan kutsua näin:
        tervehdi("Maailma");

        // Luokan ulkopuolelta täytyy aina kutsua näin:
        Tervehdys.tervehdi("Maailma");
    }
}
