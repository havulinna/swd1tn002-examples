package week3.methods;

public class Pienin {

    public static void main(String[] args) {
        int[] lukuja = new int[] { 3, 1, 2 };
        int minimi = pienin(lukuja);
        System.out.println("Pienin arvo on: " + minimi);
    }

    private static int pienin(int[] arvot) {
        int pienin = arvot[0];
        for (int arvo : arvot) {
            if (arvo < pienin) {
                pienin = arvo;
            }
        }
        return pienin;
    }
}
