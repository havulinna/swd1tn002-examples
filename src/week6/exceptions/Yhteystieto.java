package week6.exceptions;

public class Yhteystieto {

    // Epätäydellinen säännöllinen lauseke sähköpostiosoitteen tarkastamiseksi:
    private static final String EMAIL_PATTERN = "/\\S+@\\S+\\.\\S+/";

    private String nimi;
    private String email;

    public Yhteystieto(String nimi, String email) {
        this.nimi = nimi;
        this.email = email;
    }

    public void setNimi(String nimi) {
        if (nimi == null || nimi.isEmpty()) {
            throw new IllegalArgumentException("Nimi ei " + "saa olla tyhjä");
        }
        this.nimi = nimi;
    }

    public String getNimi() {
        return this.nimi;
    }

    public void setEmail(String email) {
        if (!email.matches(EMAIL_PATTERN)) {
            throw new InvalidEmailException("Virheellinen sähköpostiosoite");
        }
        this.email = email;
    }

    public String getEmail() {
        return this.email;
    }
}