package week6.exceptions;

import java.util.Scanner;

public class LueKokonaislukuUudelleen {

    public static void main(String[] args) {
        int pituus = lueKokonaisluku("Syötä pituus: ");
        System.out.println(pituus);
    }

    private static int lueKokonaisluku(String pyynto) {
        Scanner input = new Scanner(System.in);
        Integer luku = null;
        do {
            try {
                System.out.print(pyynto);
                luku = Integer.parseInt(input.nextLine());
            } catch (NumberFormatException e) {
                System.err.println("Epäkelpo kokonaisluku");
            }
        } while (luku == null);
        input.close();
        return luku;
    }
}
