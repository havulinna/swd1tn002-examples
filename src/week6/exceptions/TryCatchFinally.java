package week6.exceptions;

import java.util.Scanner;

public class TryCatchFinally {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        try {
            System.out.println("Syötä kokonaisluku:");
            Integer.parseInt(input.nextLine());
            System.out.println("Kiitos");
        } catch (NumberFormatException e) {
            System.err.println("Virheellinen luku");
        } finally {
            // Tämä lohko suoritetaan aina, riippumatta siitä,
            // tapahtuiko aikaisemmin poikkeusta.
            input.close();
        }
    }
}
