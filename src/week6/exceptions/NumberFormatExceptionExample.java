package week6.exceptions;

public class NumberFormatExceptionExample {

    public static void main(String[] args) {
        String text = "~100";

        try {
            Integer.parseInt(text);
        } catch (NumberFormatException e) {
            System.err.println("Numeron parsiminen epäonnistui.");
        }
    }
}
