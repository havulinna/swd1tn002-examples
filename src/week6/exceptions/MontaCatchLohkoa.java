package week6.exceptions;

import java.util.Scanner;

public class MontaCatchLohkoa {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String rivi;
        int numero;

        try {
            rivi = input.nextLine();
            numero = Integer.parseInt(rivi);
        } catch (NumberFormatException e) {
            System.err.println("Annettu teksti ei ole kokonaisluku");
        } catch (IllegalStateException e) {
            System.err.println("Syötteen lukeminen epäonnistui");
        }
    }
}
