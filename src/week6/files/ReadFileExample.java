package week6.files;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class ReadFileExample {

    public static void main(String[] args) {
        Path tiedostonPolku = Paths.get("tiedosto.txt");
        try {
            List<String> rivit = Files.readAllLines(tiedostonPolku);

            System.out.println("Tiedostosta luettiin rivit:");
            for (String rivi : rivit) {
                System.out.println(rivi);
            }
        } catch (IOException e) {
            System.out.println(e);
        }
    }
}
