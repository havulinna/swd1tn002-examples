package week6.files;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Sanakirja {

    private static final Path TIEDOSTO = Paths.get("sanakirja.txt");

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        boolean stop = false;

        Map<String, String> sanat = lueSanatTiedostosta(TIEDOSTO);

        while (!stop) {
            System.out.println("Anna seuraava komento:");
            String rivi = input.nextLine();
            String[] osat = rivi.split(" ");
            String komento = osat[0];

            switch (komento) {
            case "lisää":
                lisaaSanaPari(sanat, osat[1], osat[2]);
                break;
            case "kaikki":
                System.out.println(sanat);
                break;
            case "käännä":
                kaannaSana(sanat, osat[1]);
                break;
            case "poista":
                poistaSana(sanat, osat[1]);
                break;
            case "lopeta":
                stop = true;
                break;
            case "tallenna":
                tallenna(sanat, TIEDOSTO);
                break;
            default:
                System.out.println("Tuntematon komento");
                break;
            }
        }
        input.close();
    }

    private static void poistaSana(Map<String, String> sanat, String fin) {
        if (sanat.containsKey(fin)) {
            sanat.remove(fin);
            System.out.println("Poistettiin " + fin);
        } else {
            System.out.println("Sanaa " + fin + " ei löytynyt");
        }
    }

    private static void kaannaSana(Map<String, String> sanat, String fin) {
        if (sanat.containsKey(fin)) {
            System.out.println(fin + " => " + sanat.get(fin));
        } else {
            System.out.println("Sanaa " + fin + " ei löytynyt");
        }
    }

    private static void lisaaSanaPari(Map<String, String> sanakirja, String fin, String eng) {
        sanakirja.put(fin, eng);
        System.out.println("Lisättiin " + fin + " => " + eng);
    }

    private static Map<String, String> lueSanatTiedostosta(Path polku) {
        Map<String, String> sanakirja = new HashMap<String, String>();
        try {
            List<String> rivit= Files.readAllLines(polku);
            for (String rivi : rivit) {
                String[] osat = rivi.split(" => ");
                sanakirja.put(osat[0], osat[1]);
            }
            System.out.println("Luettiin " + rivit.size() + " sanaparia tiedostosta.");
        } catch (IOException e) {
            System.err.println("Tiedoston lataaminen epäonnistui: " + e.getMessage());
        }
        return sanakirja;
    }

    private static void tallenna(Map<String, String> sanakirja, Path polku) {
        try {
            ArrayList<String> rivit = new ArrayList<String>();
            for (String fin : sanakirja.keySet()) {
                String rivi = fin + " => " + sanakirja.get(fin);
                rivit.add(rivi);
            }

            Files.write(polku, rivit);
            System.out.println("Tallennettiin tiedostoon " + polku);
        } catch (IOException e) {
            System.err.println("Tallentaminen epäonnistui: " + e.getMessage());
        }
    }
}
