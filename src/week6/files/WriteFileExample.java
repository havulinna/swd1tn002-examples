package week6.files;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class WriteFileExample {

    public static void main(String[] args) {
        Path tiedostonPolku = Paths.get("kohdetiedosto.txt");
        try {
            List<String> rivit = Arrays.asList("Tämä teksti", 
                    "Kirjoitetaan tiedostoon", 
                    "Jokainen merkkijono",
                    "Omalle rivilleen");
            Files.write(tiedostonPolku, rivit);
            System.out.println("Tiedosto " + tiedostonPolku + " kirjoitettiin onnistuneesti!");

        } catch (IOException e) {
            System.out.println(e);
        }
    }
}
