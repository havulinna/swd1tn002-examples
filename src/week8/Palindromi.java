package week8;

public class Palindromi {

    public static boolean onkoPalindromi(String teksti) {
        String normalisoitu = teksti.replaceAll("\\W", "");
        String kaannetty = new StringBuilder(normalisoitu).reverse().toString();
        return !teksti.isEmpty() && normalisoitu.equalsIgnoreCase(kaannetty);
    }
}
