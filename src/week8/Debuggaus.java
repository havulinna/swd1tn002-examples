package week8;

import java.util.Scanner;

public class Debuggaus {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Kirjoita rivi tekstiä:");
        String line = s.nextLine();
        System.out.println("Kirjoita etsittävä sana:");
        String word = s.nextLine();

        if (containsIgnoreCase(line, word)) {
            System.out.println("rivi sisältää etsityn sanan");
        } else {
            System.out.println("rivi ei sisällä etsittyä sanaa");
        }
        s.close();
    }

    private static boolean containsIgnoreCase(String phrase, String keyword) {
        phrase.toLowerCase();
        keyword.toLowerCase();
        return phrase.contains(keyword);
    }
}
