package week8;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class PalindromiTest {

    @Test
    public void tyhjaMerkkijonoEiOlePalindromi() {
        boolean tulos = Palindromi.onkoPalindromi("");
        assertFalse(tulos);
    }

    @Test
    public void yksiosainenPalindromi() {
        boolean tulos = Palindromi.onkoPalindromi("saippuakivikauppias");
        assertTrue(tulos);
    }

    @Test
    public void yksiosainenEiPalindromi() {
        boolean tulos = Palindromi.onkoPalindromi("saippuamyyjä");
        assertFalse(tulos);
    }

    @Test
    public void palindromiEriKirjainkoolla() {
        boolean tulos = Palindromi.onkoPalindromi("SaippuaKiviKauppias");
        assertTrue(tulos);
    }

    @Test
    public void monisanainenPalindromi() {
        boolean tulos = Palindromi.onkoPalindromi("innostunut sonni");
        assertTrue(tulos);
    }

    @Test
    public void monisanainenEiPalindromi() {
        boolean tulos = Palindromi.onkoPalindromi("väsynyt sonni");
        assertFalse(tulos);
    }

    @Test
    public void erikoismerkkejaSisaltavaPalindromi() {
        boolean tulos = Palindromi.onkoPalindromi("Atte-kumiorava, varo imuketta!");
        assertTrue(tulos);
    }
}
