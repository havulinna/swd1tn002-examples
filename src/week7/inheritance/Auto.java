package week7.inheritance;

public class Auto {

    private String valmistaja;
    private String malli;
    private int vuosimalli;

    public Auto() {
    }

    public Auto(String valm, String malli, int vuosi) {
        this.valmistaja = valm;
        this.malli = malli;
        this.vuosimalli = vuosi;
    }

    public String getValmistaja() {
        return valmistaja;
    }

    public void setValmistaja(String valmistaja) {
        this.valmistaja = valmistaja;
    }

    public String getMalli() {
        return malli;
    }

    public void setMalli(String malli) {
        this.malli = malli;
    }

    public int getVuosimalli() {
        return vuosimalli;
    }

    public void setVuosimalli(int vuosimalli) {
        this.vuosimalli = vuosimalli;
    }
}
