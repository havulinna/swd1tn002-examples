package week7.inheritance;

public class AutoTaksiEsimerkki {

    public static void main(String[] args) {
        Auto a = new Auto("Opel", "Astra", 2000);
        Taksi t = new Taksi("Skoda", "Octavia", 2017, "H123");

        a.getMalli();
        t.getMalli();

        t.getTaksiTunnus();
    }
}
