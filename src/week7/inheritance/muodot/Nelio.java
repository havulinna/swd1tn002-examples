package week7.inheritance.muodot;

public class Nelio extends Muoto {

    private double sivunPituus;

    public Nelio(double sivunPituus) {
        super("neliö");
        this.sivunPituus = sivunPituus;
    }

    @Override
    public double pintaAla() {
        return this.sivunPituus * this.sivunPituus;
    }
}
