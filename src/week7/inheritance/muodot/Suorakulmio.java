package week7.inheritance.muodot;

public class Suorakulmio extends Muoto {

    private double leveys, korkeus;

    public Suorakulmio(double leveys, double korkeus) {
        super("suorakulmio");
        this.leveys = leveys;
        this.korkeus = korkeus;
    }

    @Override
    public double pintaAla() {
        return leveys * korkeus;
    }
}
