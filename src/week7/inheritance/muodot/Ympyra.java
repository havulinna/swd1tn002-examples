package week7.inheritance.muodot;

public class Ympyra extends Muoto {

    private double sade;

    public Ympyra(double sade) {
        super("ympyrä");
        this.sade = sade;
    }

    public double getSade() {
        return sade;
    }

    @Override
    public double pintaAla() {
        return Math.PI * Math.pow(sade, 2);
    }
}
