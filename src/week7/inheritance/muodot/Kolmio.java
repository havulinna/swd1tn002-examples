package week7.inheritance.muodot;

public class Kolmio extends Muoto {

    private double pisinSivu;
    private double korkeus;

    public Kolmio(double pisinSivu, double korkeus) {
        super("kolmio");
        this.pisinSivu = pisinSivu;
        this.korkeus = korkeus;
    }

    @Override
    public double pintaAla() {
        return pisinSivu * korkeus / 2;
    }
}
