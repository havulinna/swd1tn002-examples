package week7.inheritance.muodot;

public abstract class Muoto implements Comparable<Muoto> {

    private String nimi;

    public Muoto(String nimi) {
        this.nimi = nimi;
    }

    public String getNimi() {
        return this.nimi;
    }

    public abstract double pintaAla();

    @Override
    public int compareTo(Muoto toinen) {
        if (this.pintaAla() < toinen.pintaAla()) {
            return -1;
        } else if (this.pintaAla() == toinen.pintaAla()) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public String toString() {
        return String.format("%s (pinta-ala: %.2f)", getNimi(), pintaAla());
    }
}
