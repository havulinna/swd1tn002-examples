package week7.inheritance.muodot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MuotoLista {
    public static void main(String[] args) {

        Muoto a = new Ympyra(0.12);
        a.pintaAla();

        List<Muoto> lista = new ArrayList<Muoto>();
        lista.add(new Ympyra(12.3));
        lista.add(new Suorakulmio(4.5, 6.7));
        lista.add(new Kolmio(7.8, 5.0));
        lista.add(new Nelio(5.2));

        System.out.println(lista);
        Collections.sort(lista);
        System.out.println(lista);

        int i = 0;
        Muoto muoto = lista.get(i);
        double ala = muoto.pintaAla();

        Muoto ympyra = new Ympyra(10.0);
        // Ei toimi: double sade = ympyra.getSade();
    }

}
