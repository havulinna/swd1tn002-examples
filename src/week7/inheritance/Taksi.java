package week7.inheritance;

public class Taksi extends Auto {

    private String taksiTunnus;

    public Taksi(String valm, String malli, int vuosi, String tunnus) {
        super(valm, malli, vuosi);
        this.taksiTunnus = tunnus;
    }

    public String getTaksiTunnus() {
        return taksiTunnus;
    }

    public void setTaksiTunnus(String taksiTunnus) {
        this.taksiTunnus = taksiTunnus;
    }
}
