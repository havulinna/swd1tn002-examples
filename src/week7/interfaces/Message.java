package week7.interfaces;

public interface Message {

    public String getRecipient();
    public String getContent();
}
