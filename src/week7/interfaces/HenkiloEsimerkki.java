package week7.interfaces;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HenkiloEsimerkki {

    public static void main(String[] args) {
        Henkilo a = new Henkilo("Aapo");
        Henkilo b = new Henkilo("bertta");
        Henkilo c = new Henkilo("Cecilia");

        List<Henkilo> henkilot = new ArrayList<Henkilo>();
        henkilot.add(b);
        henkilot.add(c);
        henkilot.add(a);

        Collections.sort(henkilot);

        System.out.println(henkilot);
    }
}