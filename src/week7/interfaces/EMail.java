package week7.interfaces;

public class EMail implements Message {

    private String recipient;
    private String content;

    public EMail(String recipient, String content) {
        this.recipient = recipient;
        this.content = content;
    }

    @Override
    public String getRecipient() {
        return recipient;
    }

    @Override
    public String getContent() {
        return content;
    }
}
