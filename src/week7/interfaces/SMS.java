package week7.interfaces;

public class SMS implements Message {

    private String recipient;
    private String content;

    public SMS(String recipient, String content) {
        this.recipient = recipient;
        this.content = content;
    }

    @Override
    public String getRecipient() {
        return recipient;
    }

    @Override
    public String getContent() {
        return content;
    }

}
