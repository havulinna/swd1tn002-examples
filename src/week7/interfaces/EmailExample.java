package week7.interfaces;

public class EmailExample {

    public static void main(String[] args) {
        Message email = new EMail("user@example.com", "This is an email!");
        Message sms = new SMS("050555555", "This is an SMS!");

        email.getRecipient();
        sms.getRecipient();
    }
}
