package week7.interfaces;

public class Henkilo implements Comparable<Henkilo> {

    private String nimi;

    public Henkilo(String nimi) {
        this.nimi = nimi;
    }

    @Override
    public String toString() {
        return "Henkilö (" + this.nimi + ")";
    }

    @Override
    public int compareTo(Henkilo toinen) {
        return this.nimi.compareToIgnoreCase(toinen.nimi);
    }
}
